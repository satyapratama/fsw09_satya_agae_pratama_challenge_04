const express = require('express');
const app = express();
const { readFile } = require('fs').promises;

app.use(express.static('public')); 
// app.use('/public/images', express.static('images'));

app.get('/', async (request, response) => {

    response.send( await readFile('./public/index.html', 'utf8') );

});
app.get('/home', async (request, response) => {

    response.send( await readFile('./public/index.html', 'utf8') );

});
app.get('/sewa-mobil', async (request, response) => {

    response.send( await readFile('./public/index-sewa-mobil.html', 'utf8') );

});
app.get('*', async (request, response) => {

  response.send( await readFile('./public/404.html', 'utf8') );

});


app.listen(process.env.PORT || 3000, () => console.log(`App available on http://localhost:3000`))
