class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");

    this.driver = document.getElementById("form_driver")
    this.date = document.getElementById("form_date")
    this.time = document.getElementById("form_time")
    this.pass = document.getElementById("form_pass")    
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    };

    let d = (this.date.value+"T"+this.time.value);
    let formdate = Date.parse(d);
    let av = (this.driver.value);

    if(av == "true"){
      av = true
    }
    else(av = false)

    Car.list.forEach((car) => {
      let pass = this.pass.value
      let cardate = Date.parse(car.availableAt)

      if (car.available == av && cardate >= formdate && car.capacity >= pass){
        const node = document.createElement("div");
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      }
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
