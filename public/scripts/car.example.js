class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    const avat = this.availableAt
    let day = avat.toDateString()
    return `
      <div class="card car_card h-100">
        <img src="${this.image}" class="card-img-top" alt="${this.manufacture}" max-height="1rem">
        <div class="card-body">
          <h5 class="card-title">${this.manufacture} ${this.model}</h5>
          <h6 class="card-subtitle">Rp. ${this.rentPerDay}/Day</h6>
          <p class="card-text"></p>
          <p class="card-text"><b>${this.description}</b></p>
          <p class="card-text"><i class="bi bi-person"></i> <b>${this.capacity}</b></p>
          <p class="card-text"><i class="bi bi-calendar"></i> <b>${day}</b></p>
          
        </div>
      </div>
    `;
  }
}
